# Ensemble-based volcanic dispersal across multiple scales

**PD leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/csic.png?inline=false" width="80">  -  <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/IMO.png?inline=false" width="80">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/INGV.png?inline=false" width="35">



**Codes:** [FALL3D](https://gitlab.com/cheese5311126/codes/fall3d)

**Derived Simulation Cases:** [SC5.1](https://gitlab.com/cheese5311126/simulation-cases/sc5.1), [SC5.2](https://gitlab.com/cheese5311126/simulation-cases/sc5.2)

## Description

Volcanic eruptions emit aerosols and particles of sizes ranging from centimeters to microns that impact at varying distances. Depending on the range, the atmospheric dispersal and deposition mechanisms are controlled by winds and meteorological phenomena occurring at scales from local (e.g. orography and breeze effects) to global (general
atmospheric circulation). The developments in ChEESE-1P included a PVHA workflow and a forecast strategy generally
valid in the mesoscale range. This restriction presents shortcomings when volcanoes are in or very close to populated areas. For example, during the eruption at La Palma, the air quality forecasts were very challenging and often failed due to complex local-scale wind patterns not well captured by the models, including wind convection phenomena caused by the presence of widespread lava flows at high temperatures.

## Objectives

The main objectives of this PD are:

1. Include a wind downscaling strategy to capture local-scale effects and make volcanic forecasts realistic below km-scale resolutions.
2. Enable a model nesting strategy for telescopic PVHA,
thereby leading to high-resolution accurate forecasts. This aspect is pivotal for a proper response during crises and prompt early-warning of severe pollution conditions expected in populated regions around volcanoes [(SC5.2)](https://gitlab.com/cheese5311126/simulation-cases/sc5.2).
3. On the other hand, the multiscale approach will allow for the first European tephra hazard map at scale covering, simultaneously, long-range dispersal and short-range fallout telescopically [(SC5.1)](https://gitlab.com/cheese5311126/simulation-cases/sc5.1) to be integrated in the EPOS Volcanic Observations TCS (VO-TCS).
